# devs.tf configures the Apigee developers

locals {
  # Load all devs into a single object keyed by dev id.
  users = {
    for dev_id in fileset("${local.export_path}/devs", "*") :
    (dev_id) => jsondecode(file("${local.export_path}/devs/${dev_id}")) if substr(dev_id, -18, -1) != "@devteam.apigee.io"
  }

  // a mapping of developer team id in apigee edge to developer team id in apigee x,
  // this is manually maintained as we need to clickops create teams within the
  // production portal as there is no means to create these via the Apigee API.
  apigee_edge_to_apigee_x_team_ids = {
    "62a84d9a-5995-41a8-9785-009fd4fddf3f" = "b1cd85f5-96a6-4dd4-b66d-11a1aee07fa4",
    "fb95c4fb-6593-41de-bcf2-d7f662f330df" = "cbff4486-14ff-45de-b220-15ece978c066",
    "87d2b254-3190-498e-bd88-c42f65bbea10" = "54ce6662-7c8e-4a4c-8178-9569aaea07bd",
    "b81bbe3a-2386-4ecd-a986-822fea7aece6" = "72f35bbd-6a32-4489-a45c-5828cf5d6041",
    "c4499519-d597-46cf-a3b2-a592e209b7f2" = "6e44edad-9a1e-4839-a81e-ceadf3497754",
    "8c0bae25-ec54-4cf1-91a8-31aa41352583" = "eb94f41d-2346-4f7f-8544-89c867db3ec7",
    "c93c84ef-e7b4-4bd3-81e5-ab4dcf0dafc3" = "ab997a99-b46d-4f3f-b6e4-f608a99fad43",
    "6194eb1a-e10a-4351-9145-df0911336a92" = "3bd83f88-197b-463e-b620-5d0b411119df",
    "4c22af57-e364-49aa-bf50-8a628a767faf" = "88cf4283-f5a4-4db4-9dd8-e9f7979cb4a2",
    "dd57770a-96a8-4399-b453-b81a99c98cef" = "657e8437-12a8-46d1-b125-41f7aa300900",
    "03bc5839-7b1c-4558-b853-67465b852025" = "9efe2cb9-3c41-4342-bb39-31011e77743b",
    "90e7071e-46ca-494e-aa36-a3d2fe8fd6a3" = "9dc36ac2-ef07-4e18-9787-9734fc14ae22",
    "7974cbf4-d08f-4705-8b3c-924aa9c1c773" = "602b2d9f-3bef-4cb4-8af8-0a3e37dcfcd6",
    "b21bc86c-3a9b-4942-846e-5ba3ac80b5a8" = "a04afe40-1928-4a41-a14f-a78332ec2e07",
    "5fa0791a-510f-47eb-b5aa-4228fd6e3705" = "a3929515-1e38-47ab-9afd-50c8e91108df",
    "ea227c0b-707d-4ca3-8df8-8c84c27169ca" = "2666c78d-b998-4e77-872c-8a9e9571ff71",
    "ac841e9e-9f25-412a-999f-836cd1d7cc29" = "5acd6608-0f77-4020-8231-c6c27c291a54",
    "bec08161-3671-4661-a87d-c68f02a56f01" = "1247c32d-a75f-467b-8921-0027d22ccd6e",
    "405c663f-2b79-4a53-b951-aa920fb658aa" = "67834637-92e2-48f7-8406-6a429db8510d",
    "bdb448b9-e036-4b3b-93f8-71129613ca45" = "9b1adc61-c3ad-4736-90d1-c22df77d8ae3",
    "c81e15a3-50b5-4f18-963c-1b822ebd2910" = "caafd2a7-ca9d-427a-83b0-4918dd7b846e",
    "26c0a0f3-de12-47d7-8908-a38f783b81d1" = "c05c693b-5a82-4ab9-b90b-8538ef30a8d4",
    "c9c0c145-f10b-49e3-83a1-de64912db806" = "edc8929f-5787-44f8-8af6-7da33c7d58b5",
    "ecae7774-9fea-416d-bdc9-1886c8bb9f7c" = "b186daff-dab4-4756-935a-8a76ac1127dc",
    "f1baa5ca-1b1b-40a5-b222-e9c89684cc6c" = "5cd9f042-e443-40af-b06b-8ac8c52ec991",
    "d2a85c00-4742-4b60-8c28-08a748a2237e" = "3eca5bcb-a84c-42af-89ff-1c02b55d7812",
    "cafc8cdb-640a-4e85-966d-aff7b6aafc9d" = "ac58c9f3-e99a-46b7-a2af-6ca81d1e786f",
    "fd9b2559-63c7-4951-bc45-58bd8327861a" = "067db8b2-4944-44d5-9697-70a5f8d750a5",
    "a01172fd-5957-4886-a129-549cef84569c" = "3f7556f7-93c1-4211-a673-f5856960d4d6",
    "8b89270c-0c42-488f-a722-4ff0cf013691" = "b6b4d0ba-8aac-4204-ae42-337422d11b73",
    "ecfcc5ea-785a-4b79-8f99-9a0619605f13" = "f4693c93-f3f9-42a7-b9f7-1482ed192624",
    "7e85aa7e-231c-456d-9628-7684be9a433b" = "4c05dfcb-a84c-4197-8971-7ca8eecd6aa7",
    "b9829727-27ee-4817-b39c-a6e30aa3cc96" = "069f2b05-c528-44f3-94bf-3fef5c27da73",
    "cbe3a319-d660-4533-ad9e-519818edcdfd" = "480c31af-1f0a-4b17-8ae9-b4cbd3e1d85b"
  }

  # Load all dev teams into a single object keyed by dev id - we map these against the dev ids
  # map above, to match up clickops created teams based on the uuids of the export from apigee
  # edge.
  all_dev_teams = {
    for dev_id in fileset("${local.export_path}/devs", "*") :
    (dev_id) => jsondecode(file("${local.export_path}/devs/${dev_id}")) if substr(dev_id, -18, -1) == "@devteam.apigee.io"
  }

  # a map of developer team id to developer email for developer teams - with the developer
  # email relating to a developer team id that has been clickops created within Apigee X
  apigee_x_dev_teams = {
    for dev_id, v in local.all_dev_teams :
    (dev_id) => {
      email = format("%s@devteam.apigee.io", local.apigee_edge_to_apigee_x_team_ids[trimsuffix(v.email, "@devteam.apigee.io")])
    }
  }

  # a mapping of developer id to developer email - used to link up apps to developers
  developer_id_to_developer_email = {
    for dev_id, user in merge(local.apigee_x_dev_teams, local.users) :
    (dev_id) => user.email
  }
}

resource "apigee_developer" "import" {
  for_each = local.users

  email      = each.value.email
  first_name = each.value.firstName
  last_name  = each.value.lastName
  user_name  = each.value.userName

  attributes = {
    for attr in each.value.attributes : attr.name => attr.value if attr.value != ""
  }
}
