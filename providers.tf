# providers.tf contains configuration for all the terraform providers used this
# configuration.

# Providers which act as the current Google user.
provider "google" {
  project = local.project
  region  = local.region
}

provider "google-beta" {
  project = local.project
  region  = local.region
}

# Apigee provider configuration.
provider "apigee" {
  organization = local.project
  access_token = local.apigee_admin_access_token
  server       = "apigee.googleapis.com"
}
