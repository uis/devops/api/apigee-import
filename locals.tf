# locals.tf contain local configuration

locals {
  # Google-related settings.
  project = "api-prod-a3dc87f7"
  region  = "europe-west2"

  # Location of export data from apigee-migrate-tool.
  export_path = "${path.module}/apigee-migrate-tool/data"
}
