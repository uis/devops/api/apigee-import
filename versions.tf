# versions.tf specifies minimum versions for providers and terraform.

terraform {
  # Specify the required providers, their version restrictions and where to get
  # them.
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.36"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.36"
    }

    apigee = {
      # Pin an exact version of the apigee provider since semver indicates 0.x
      # releases need not be compatible.
      source  = "scastria/apigee"
      version = "0.1.41"
    }
  }

  required_version = ">= 0.14"
}
