# apigeecreds.tf configures credentials for Apigee X.

# Obtain a token for the current user to use as the Apigee user.
data "google_client_config" "client" {}

locals {
  apigee_admin_access_token = data.google_client_config.client.access_token
}
