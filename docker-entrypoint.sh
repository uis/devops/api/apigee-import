#!/usr/bin/env bash

# Exit on error
set -e

# Check for required environment variables.
if [ -z "${APIGEE_EDGE_ADMIN_USERNAME}" ]; then
  echo "APIGEE_EDGE_ADMIN_USERNAME must be defined." >&2
  exit 1
fi

if [ -z "${APIGEE_EDGE_ADMIN_PASSWORD}" ]; then
  echo "APIGEE_EDGE_ADMIN_PASSWORD must be defined." >&2
  exit 1
fi

if [ ! -f "${GOOGLE_APPLICATION_CREDENTIALS}" ]; then
  echo "${GOOGLE_APPLICATION_CREDENTIALS} must be service account credentials." >&2
  exit 1
fi

# Initialise terraform backend. Plugins were already downloaded as part of
# terraform build.
terraform init -get=false

# Write apigee migrate tool config.
cat > "apigee-migrate-tool/config.js" <<EOI
module.exports = {
  from: {
    version: '19.04',
    url: 'https://api.enterprise.apigee.com',
    userid: '${APIGEE_EDGE_ADMIN_USERNAME}',
    passwd: '${APIGEE_EDGE_ADMIN_PASSWORD}',
    org: 'cam',
    env: 'prod'
  },
  // Note: we don't use "to" in our config but it needs to be present. We use
  // mock values here.
  to: {
    version: '19.04',
    url: 'https://api.enterprise.apigee.com',
    userid: 'admin@example.invalid',
    passwd: 'SuperSecret123!9',
    org: 'my-org',
    env: 'production'
  }
};
EOI

# Run migrate tool. We don't use "-v" since that writes credentials directly to
# the logs(!)
pushd apigee-migrate-tool
npx grunt exportDevs
npx grunt exportApps
popd

# Show terraform plan
terraform plan -out=plan.tfplan

# Apply terraform
terraform apply -auto-approve plan.tfplan
