# It it easier to install npm/node in the Logan terraform image than to install
# all the terraform, gcloud, etc tooling in the official node image.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform:0.14

# Install npm and node.
RUN apk add --no-cache npm nodejs

# Clone migrate tool and install.
RUN git clone --depth 1 https://github.com/apigeecs/apigee-migrate-tool \
    && cd apigee-migrate-tool && npm i

# Copy configuration.
ADD ./ ./

# Use default location for terraform data.
ENV TF_DATA_DIR=

# Initialise terraform plugins but not the backend. (The backend config requires
# credentials.)
RUN terraform init -backend=false

# Set entrypoint.
ENTRYPOINT ["./docker-entrypoint.sh"]
