# backend.tf configures the teraform remote state backend.
#
terraform {
  backend "gcs" {
    # This bucket has been click-opsed for this migration process in the
    # production Apigee project.
    bucket = "apigee-migrate-state-225grh5h1j"
    prefix = "terraform/migrate"
  }
}
