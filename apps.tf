# apps.tf configures the Apigee apps

locals {
  # Load all apps and map them into an object keyed by
  # "[developer id]/[app name]".
  all_apps = {
    for file in fileset("${local.export_path}/apps", "*/*") :
    (file) => merge(
      jsondecode(file("${local.export_path}/apps/${file}")),
      {
        # Extract email from apigee_developer resource which also
        # introduces a dependency on the developer being created before the app.
        developer_email = local.developer_id_to_developer_email[split("/", file)[0]]
        app_key         = file
      }
    )
  }
}

resource "apigee_developer_app" "import" {
  for_each = local.all_apps

  developer_email = each.value.developer_email

  name         = each.value.name
  callback_url = each.value.callbackUrl

  attributes = {
    for attr in each.value.attributes : attr.name => attr.value if attr.value != ""
  }
}

locals {
  # Construct a map of all credentials keyed by
  # "[developer id]/[app name]/[index]".
  all_app_credentials = {
    for v in flatten([
      for app_key, app in local.all_apps : [
        for index, credential in app.credentials : {
          key = "${app_key}/${index}"
          value = merge(
            credential,
            {
              # Extract name from apigee_developer_app resource which also
              # introduces a dependency on the app being created before the
              # credential.
              developer_app_name = apigee_developer_app.import[app.app_key].name
              developer_email    = app.developer_email
            }
          )
        }
      ]
    ]) : v.key => v.value
  }
}

resource "apigee_developer_app_credential" "import" {
  for_each = local.all_app_credentials

  developer_email    = each.value.developer_email
  developer_app_name = each.value.developer_app_name

  consumer_key    = each.value.consumerKey
  consumer_secret = each.value.consumerSecret

  api_products = [for v in each.value.apiProducts : v.apiproduct]

  scopes = each.value.scopes

  attributes = {
    for attr in each.value.attributes : attr.name => attr.value if attr.value != ""
  }
}
