# Apigee X import tool

This is a one-off tool written in terraform(!) to import developers,
applications and application credentials from Apigee Edge to Apigee X.

Some important notes:

* The *default* account used by the `gcloud` SDK must be an Apigee admin.
* The destination org is hard-coded in [locals.tf](locals.tf).
* Do **not** run this tool via logan, run it with bare terraform.

## Click-opsed infrastructure

Due to the temporary nature of this migration, some things which could have in
principle been automated have been click-opsed:

* A bucket named "apigee-migrate-state-225grh5h1j" has been created in the
  Apigee production Google project and the Apigee Admin service account has been
  given owner rights over it. This bucket is used to store the terraform state.
* Credentials for the Apigee Admin service account have been created and added
  to the `GOOGLE_APPLICATION_CREDENTIALS` CI variable in this repo.
* Credentials for the Apigee Edge terraform account have been added to the
  `APIGEE_EDGE_ADMIN_{USERNAME,PASSWORD}` CI variables in this repo.
* A scheduled CI job has been created in this repo to run the migration at 10
  minute intervals.

## Preparation

1. Clone the [Apigee migration
   tool](https://github.com/apigeecs/apigee-migrate-tool) locally.
2. Update the `config.js` file in that repo so that the org and credentials in
   the "from" instance match the [production settings in
   1password](https://start.1password.com/open/i?a=D3ATZUD36RDHLDKSVJUQZWGORQ&v=rzdugks5meinz5oc772yudf3ra&i=ls3opzv6c3pe4rltd63oxze76a&h=uis-devops.1password.eu).
3. Install the tool via `npm i`.
4. Run an export via `npx grunt exportDevs -v` and `npx grunt exportApps -v`.

## Performing an import

Assuming your `gcloud` SDK user has the appropriate rights you should be able to
perform a `terraform init` and `terraform apply` as per usual. All resources are
configured in the `default` workspace.

## Approvals

There doesn't appear to be a way to set application API product approvals in via
the API and so one will need to manually edit and approve all apps which use
sensitive APIs :(.
