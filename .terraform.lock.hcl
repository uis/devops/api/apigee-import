# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.89.0"
  constraints = ">= 3.36.0"
  hashes = [
    "h1:+C8w9B9aJMpALgmFnqZuHJk/pEDTHWkZ+IrNCEmjB+k=",
    "h1:8wOU/o8YYxjtcJMF2y+kvyJFWVjJryM5Aug7FH3WM2c=",
    "zh:208317c042622a4f761e96cb09871c57d7601ff5f21fc5952567d3940203cd93",
    "zh:227eb06659d5ecbc679de97ce130e8d7400ed0f04fb9e4d13869057ae2b1113e",
    "zh:267809935e07e78acfc28f1a0aab03d659c777bff5300ae818d76578eb3f3034",
    "zh:59c43802370a6e0893be34c50f036f4bc8dbd6a77475846f2c5ea0a2d522dbfa",
    "zh:66a482ef171381ca5d1d5037216db1688381cd72ba939a416b5c81e61352bac6",
    "zh:7314db9b6c529dc6d391ca852333229dc79ac2be460dc7410fb73cb038cd0672",
    "zh:79c051035bc5f0e019e650103109fab17051d1d33bffa4040bae5262820577cb",
    "zh:79f775b07d070a892fdde9c1eff01fe0a4473be2f8b188aea80ac709ce666352",
    "zh:7ab4db6ff5cb9ba8b7fbf17b789e9172548ef029a5c48c04cfa1f6c4926aaa88",
    "zh:9a0589b2e5da783158e81a4cfc611951a6248b456893cdfac3ded041d25ea6b2",
    "zh:f2a4a0a875ddff79016cb3f7f6705ef263caabb312836f999bfe56decdac673e",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "3.89.0"
  constraints = ">= 3.36.0"
  hashes = [
    "h1:iOQAgndi2gI4cuGuXUsI2M3bYzbRIHf1GlEL1NCgKik=",
    "h1:kT38l+vDgN6aY+y9QBR1FQMQsUCjteVxYEJ4l36AtoU=",
    "zh:06002648b127aa5741fcddbf33088553ec0845fc9eb024605b491ff79e9cc74d",
    "zh:07d1b067185ff1deac958de2e3414553d265e000b34cf5014524898330e10b4f",
    "zh:3e3c3836df68e1ff01140d36af7e32b84232f4089666a4074a505793b0b73910",
    "zh:42f731398e7ca5d5850eec3de68a847455210a27510e83b85213ffb713432ce4",
    "zh:5dfeeddde4029a999cb82444659f38b96804d105d138614abb7e8bd05cfd7f67",
    "zh:725fac8088cc00b17985a43e40454bc9b8bcca2c50059b512ad24400a2a498fb",
    "zh:978d7f00c20ad8dda0d53e406f1ea756c59fb1a23d1794a3633ab203a4547aef",
    "zh:b4d3d8276d1679250576f4394a5a6c4dbf4543f4829e351fb0d344b88990b310",
    "zh:c6ce14e928fe755fb05c636e8bbc3dc3fe471103e5f5219c5dd924b30eea1022",
    "zh:f6a2d6815884f55f4cf0afb3ee18b51a73682ba0fec0ff0387b988248eb570fd",
    "zh:fb3dd2c29f78af8861ca1f5ef741dcbe9d47fe3d4a9445be4cbef82a42c3250b",
  ]
}

provider "registry.terraform.io/scastria/apigee" {
  version     = "0.1.41"
  constraints = "0.1.41"
  hashes = [
    "h1:Vy7PeTV2lJgLjx+8/Z1W0eZQfXJNYeM6I2AeL1f5V0E=",
    "h1:bxZwzC+QDmeBh0FlJAjjdeyq6+JCZnHEvc+xRz8JE8o=",
    "zh:0464b82dc8a4f6d7fa039a727f0eeb742d33f5c92e535b089b7ccbb8d08806bb",
    "zh:1da3140415172fac5a48ce94bd1ebffa0ae8f44b9517526b14f40f14f279a95e",
    "zh:30f0dd939f702ff9646179699d6105199a122bc3293f3bc4bc51989124db96ca",
    "zh:3406c45e74f371824ab59916625cab79ac92853c788a38af87c9c3d2906936f8",
    "zh:363f1bc205c4f429163bc773db80f11a4ee5406cf88c934f086e50e07757e196",
    "zh:3e87af003f79d7e48b28a058711cbd621b17cee165c31e45e7ed5c955700f149",
    "zh:43d80c782b8349ea6e4f3c56b57f687776c2efaefdb950afd13335a550956f2e",
    "zh:7796c9e6fc96a647d31459e3fddf6abe7c384c23f85df802cce1150de1270c51",
    "zh:9b002c0be6fceeb35cd57fe35c2bd7c2f8d3193a09bfda69631161521485d76f",
    "zh:a39e4147483d47cdfcace5b39ad21dc411e2a3c85053e76afb52a095af7feb8f",
    "zh:c5e1f6620591156dc9ec1f8126aa280c75b0d8b070674f85ce5516713cca6166",
    "zh:ca726802332ec4d1a2fd690585c3b3d1aa1dbc6772bde88d7211315152057d0c",
  ]
}
